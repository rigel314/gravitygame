package game

import (
	"encoding/binary"

	"golang.org/x/mobile/exp/f32"
)

var triangleData = f32.Bytes(binary.LittleEndian,
	0.0, 1.0, 0.0, // top left
	0.0, 0.0, 0.0, // bottom left
	1.0, 0.0, 0.0, // bottom right
)

var squareData = f32.Bytes(binary.LittleEndian,
	0.0, 1.0, 0.0, // top left
	1.0, 1.0, 0.0, // top right
	0.0, 0.0, 0.0, // bottom left
	1.0, 0.0, 0.0, // bottom right
)

const vertexShader = `
#version 100
uniform vec2 offset;
uniform float scale;
uniform vec2 devScale;
uniform float zIndex;

attribute vec4 inputVert;
void main() {
	// offset comes in with x/y values between 0 and 1.
	// inputVert bounds are -1 to 1.
	vec4 offset4 = vec4((2.0*offset.x-1.0), (1.0-2.0*offset.y), zIndex, 0);
	vec4 scale4 = vec4(scale*devScale.x, scale*devScale.y, 1, 1);
	gl_Position = inputVert*scale4 + offset4;
}`

const fragmentShader = `
#version 100
precision mediump float;
uniform vec4 color;
void main() {
	gl_FragColor = color;
}`
