package game

import "gitlab.com/rigel314/gravityGame/engine"

// Player is a player object
type Player struct {
	engine.Object
}

// Draw sets color and wraps Object Draw
func (p *Player) Draw() {
	p.Object.Color = engine.RGBA{R: .9, G: .9, B: 0, A: 1}
	p.Object.Draw()
}

// Target is a goal object
type Target struct {
	engine.Object
}

// Draw sets color and wraps Object Draw
func (p *Target) Draw() {
	p.Object.Color = engine.RGBA{R: 1, G: 1, B: 1, A: 1}
	p.Object.Draw()
}
