package game

import (
	"log"

	"gitlab.com/rigel314/gravityGame/engine"
	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/gl"
)

//go:generate go run ./cmd/genCircleVerticies -p ./cmd/genCircleVerticies/circleVerticies.tmpl

// Game holds the state for a game
type Game struct {
	glctx     gl.Context
	program   gl.Program
	tribuf    gl.Buffer
	circbuf   gl.Buffer
	squarebuf gl.Buffer

	touchX   float32
	touchY   float32
	widthPx  float32
	heightPx float32

	green     float32
	friction  float32
	velocityX float32
	velocityY float32
	circScale float32

	objs   []engine.Drawer
	player *Player
	target *Target
	gravs  []*Grav
}

// Defaults will set some internal fields to good defaults
func (g *Game) Defaults() {
	g.friction = 1.03
	g.green = .75
	g.circScale = .74
}

// InitContext should be called to initialize a gl context, but not reset Game state
func (g *Game) InitContext(glctx gl.Context) {
	var err error
	g.glctx = glctx
	g.program, err = glutil.CreateProgram(g.glctx, vertexShader, fragmentShader)
	if err != nil {
		log.Printf("error creating GL program: %v", err)
		return
	}
	g.glctx.Enable(gl.DEPTH_TEST)

	g.tribuf = g.glctx.CreateBuffer()
	g.glctx.BindBuffer(gl.ARRAY_BUFFER, g.tribuf)
	g.glctx.BufferData(gl.ARRAY_BUFFER, triangleData, gl.STATIC_DRAW)

	g.circbuf = g.glctx.CreateBuffer()
	g.glctx.BindBuffer(gl.ARRAY_BUFFER, g.circbuf)
	g.glctx.BufferData(gl.ARRAY_BUFFER, circleData, gl.STATIC_DRAW)

	g.squarebuf = g.glctx.CreateBuffer()
	g.glctx.BindBuffer(gl.ARRAY_BUFFER, g.squarebuf)
	g.glctx.BufferData(gl.ARRAY_BUFFER, squareData, gl.STATIC_DRAW)

	g.objs = make([]engine.Drawer, 0, 2)

	g.player = &Player{}
	g.player.GlInit(g.glctx, g.program, g.circbuf, gl.TRIANGLE_FAN, 3, len(circleData)/3)
	g.player.Scale = .05
	g.player.Pos = engine.XY{X: 500, Y: 1000}
	g.player.ZIndex = -1
	g.objs = append(g.objs, g.player)

	g.target = &Target{}
	g.target.GlInit(g.glctx, g.program, g.squarebuf, gl.TRIANGLE_STRIP, 3, 4)
	g.target.Scale = .05
	g.target.Pos = engine.XY{X: 500, Y: 2000}
	g.objs = append(g.objs, g.target)

	g.gravs = make([]*Grav, 0, 2)

	grav := &Grav{Mass: 1}
	grav.GlInit(g.glctx, g.program, g.circbuf, gl.TRIANGLE_FAN, 3, len(circleData)/3)
	grav.Scale = .1
	grav.Pos = engine.XY{X: 1000, Y: 1500}
	g.gravs = append(g.gravs, grav)
	g.objs = append(g.objs, grav)

	grav = &Grav{Mass: -1}
	grav.GlInit(g.glctx, g.program, g.circbuf, gl.TRIANGLE_FAN, 3, len(circleData)/3)
	grav.Scale = .1
	grav.Pos = engine.XY{X: 1000, Y: 2000}
	g.gravs = append(g.gravs, grav)
	g.objs = append(g.objs, grav)

	grav = &Grav{Mass: 0}
	grav.GlInit(g.glctx, g.program, g.circbuf, gl.TRIANGLE_FAN, 3, len(circleData)/3)
	grav.Scale = .1
	grav.Pos = engine.XY{X: 1000, Y: 2500}
	g.gravs = append(g.gravs, grav)
	g.objs = append(g.objs, grav)
}

// Paint should be called to draw a frame of the game
func (g *Game) Paint() {
	g.update()

	g.glctx.ClearColor(0, 0, 0, 1)
	g.glctx.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	for _, o := range g.objs {
		o.Draw()
	}
}

// TouchEvent should be called when the window is touched or clicked
func (g *Game) TouchEvent(x, y float32) {
	g.touchX = x
	g.touchY = y
	g.velocityX = g.touchX - g.player.Pos.X
	g.velocityY = g.touchY - g.player.Pos.Y
	g.circScale = (g.heightPx - g.touchY) / g.heightPx
	// log.Println("pos:", g.player.Pos)
}

// SizeEvent should be called when the window is resized or shortly after init
func (g *Game) SizeEvent(w, h int) {
	g.widthPx = float32(w)
	g.heightPx = float32(h)
	g.touchX = float32(w) / 2
	g.touchY = float32(h) / 2
	g.player.Pos.X = g.touchX
	g.player.Pos.Y = g.touchY

	for _, o := range g.objs {
		o.SetDimensions(engine.XY{X: g.widthPx, Y: g.heightPx})
	}

	log.Println("here")
}

// DestroyContext should be called to destroy the previously initialized context
func (g *Game) DestroyContext() {
	g.glctx.DeleteProgram(g.program)
	g.glctx.DeleteBuffer(g.tribuf)
	g.glctx.DeleteBuffer(g.circbuf)
}
