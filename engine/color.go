package engine

// RGBA holds a color
type RGBA struct {
	R, G, B, A float32
}

// ClampWrap makes c between min and max with wrap around.  This will apply the max-min difference only once, so a value can still be out of range.
func (c *RGBA) ClampWrap(min, max RGBA) {
	if c.R < min.R {
		c.R += max.R - min.R
	}
	if c.G < min.G {
		c.G += max.G - min.G
	}
	if c.B < min.B {
		c.B += max.B - min.B
	}
	if c.A < min.A {
		c.A += max.A - min.A
	}
	if c.R > max.R {
		c.R -= max.R - min.R
	}
	if c.G > max.G {
		c.G -= max.G - min.G
	}
	if c.B > max.B {
		c.B -= max.B - min.B
	}
	if c.A > max.A {
		c.A -= max.A - min.A
	}
}

// ClampWrapN forces c between min and max with wrap around.  This will continually apply the max-min difference until the value has been clamped.
func (c *RGBA) ClampWrapN(min, max RGBA) {
	for c.R < min.R {
		c.R += max.R - min.R
	}
	for c.G < min.G {
		c.G += max.G - min.G
	}
	for c.B < min.B {
		c.B += max.B - min.B
	}
	for c.A < min.A {
		c.A += max.A - min.A
	}
	for c.R > max.R {
		c.R -= max.R - min.R
	}
	for c.G > max.G {
		c.G -= max.G - min.G
	}
	for c.B > max.B {
		c.B -= max.B - min.B
	}
	for c.A > max.A {
		c.A -= max.A - min.A
	}
}

// Add adds two colors element-wise, and can end up non-normalized
func (c *RGBA) Add(x RGBA) {
	c.R += x.R
	c.G += x.G
	c.B += x.B
	c.A += x.A
}
