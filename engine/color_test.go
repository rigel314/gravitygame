package engine_test

import (
	"testing"

	"gitlab.com/rigel314/gravityGame/engine"
)

func TestRGBA_ClampWrap_Nominal(t *testing.T) {
	c := engine.RGBA{R: .5, G: .5, B: .5, A: .5}
	ctruth := c
	c.ClampWrap(engine.RGBA{0, 0, 0, 0}, engine.RGBA{1, 1, 1, 1})
	if c != ctruth {
		t.Fail()
	}
}
func TestRGBA_ClampWrap_Over(t *testing.T) {
	c := engine.RGBA{R: 1.5, G: .5, B: .5, A: .5}
	ctruth := engine.RGBA{R: .5, G: .5, B: .5, A: .5}
	c.ClampWrap(engine.RGBA{0, 0, 0, 0}, engine.RGBA{1, 1, 1, 1})
	if c != ctruth {
		t.Fail()
	}
}

func TestRGBA_ClampWrap_Under(t *testing.T) {
	c := engine.RGBA{R: -.5, G: .5, B: .5, A: .5}
	ctruth := engine.RGBA{R: .5, G: .5, B: .5, A: .5}
	c.ClampWrap(engine.RGBA{0, 0, 0, 0}, engine.RGBA{1, 1, 1, 1})
	if c != ctruth {
		t.Fail()
	}
}

func TestRGBA_ClampWrap_JustUnder(t *testing.T) {
	c := engine.RGBA{R: -.1, G: .5, B: .5, A: .5}
	ctruth := engine.RGBA{R: .9, G: .5, B: .5, A: .5}
	c.ClampWrap(engine.RGBA{0, 0, 0, 0}, engine.RGBA{1, 1, 1, 1})
	if c != ctruth {
		t.Fail()
	}
}

func TestRGBA_ClampWrapN_Nominal(t *testing.T) {
	c := engine.RGBA{R: .5, G: .5, B: .5, A: .5}
	ctruth := c
	c.ClampWrapN(engine.RGBA{0, 0, 0, 0}, engine.RGBA{1, 1, 1, 1})
	if c != ctruth {
		t.Fail()
	}
}
func TestRGBA_ClampWrapN_Over(t *testing.T) {
	c := engine.RGBA{R: 1.5, G: .5, B: .5, A: .5}
	ctruth := engine.RGBA{R: .5, G: .5, B: .5, A: .5}
	c.ClampWrapN(engine.RGBA{0, 0, 0, 0}, engine.RGBA{1, 1, 1, 1})
	if c != ctruth {
		t.Fail()
	}
}

func TestRGBA_ClampWrapN_Under(t *testing.T) {
	c := engine.RGBA{R: -.5, G: .5, B: .5, A: .5}
	ctruth := engine.RGBA{R: .5, G: .5, B: .5, A: .5}
	c.ClampWrapN(engine.RGBA{0, 0, 0, 0}, engine.RGBA{1, 1, 1, 1})
	if c != ctruth {
		t.Fail()
	}
}
