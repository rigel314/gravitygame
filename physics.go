package game

func (g *Game) update() {
	g.player.Pos.X += g.velocityX / 60
	g.player.Pos.Y += g.velocityY / 60

	g.velocityX /= g.friction
	g.velocityY /= g.friction
}
