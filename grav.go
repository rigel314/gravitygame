package game

import "gitlab.com/rigel314/gravityGame/engine"

// Grav is a gravity object
type Grav struct {
	engine.Object

	Mass float32

	color engine.RGBA
}

// Draw sets color and wraps Object Draw
func (p *Grav) Draw() {
	var actualColor engine.RGBA
	if p.Mass < 0 {
		colorOffset := engine.RGBA{R: 0, G: .25 / 3, B: 0, A: 0}

		p.color.R -= .004
		p.color.B = 0
		p.color.G -= .004

		p.color.ClampWrap(engine.RGBA{R: .75, G: .5, B: 0, A: 1}, engine.RGBA{R: 1, G: .75, B: 0, A: 1})
		actualColor = p.color
		actualColor.Add(colorOffset)
		actualColor.ClampWrap(engine.RGBA{R: .75, G: .5, B: 0, A: 1}, engine.RGBA{R: 1, G: .75, B: 0, A: 1})
	} else if p.Mass > 0 {
		colorOffset := engine.RGBA{R: 0, G: 0, B: 0, A: 0}

		p.color.R = 0
		p.color.B = 1
		p.color.G -= .0025

		p.color.ClampWrap(engine.RGBA{R: 0, G: .5, B: .75, A: 1}, engine.RGBA{R: 0, G: .75, B: 1, A: 1})
		actualColor = p.color
		actualColor.Add(colorOffset)
		actualColor.ClampWrap(engine.RGBA{R: 0, G: .5, B: .75, A: 1}, engine.RGBA{R: 0, G: .75, B: 1, A: 1})
	} else {
		actualColor = engine.RGBA{R: .75, G: 0, B: .75, A: 1}
	}

	p.Object.Color = actualColor
	p.Object.Draw()
}
