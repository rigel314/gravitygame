package main

import (
	"flag"
	"log"
	"math"
	"os"
	"text/template"
)

var tmplPath = flag.String("p", "", "path to template file")

const nPoints = 32

func main() {
	flag.Parse()

	pts := make([][]float32, 0, nPoints+2)

	pts = append(pts, []float32{0, 0, 0})

	for i := 0; i < nPoints+1; i++ {
		pt := make([]float32, 3)
		pt[0] = float32(math.Cos(float64(i) * 2 * math.Pi / nPoints))
		pt[1] = float32(math.Sin(float64(i) * 2 * math.Pi / nPoints))
		pts = append(pts, pt)
	}

	// pts := [][]float32{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}

	t := template.Must(template.ParseFiles(*tmplPath))

	f, err := os.Create("circleVertices-gen.go")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	// Execute the template for each recipient.
	err = t.Execute(f, pts)
	if err != nil {
		log.Println("executing template:", err)
	}
}
